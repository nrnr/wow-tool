(() => {
  let futures = [];

  const scripts = [
    "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.min.js",
  ];

  scripts.forEach((script) => {
    const tag = document.createElement("script");
    tag.type = "text/javascript";
    tag.src = script;
    const future = new Promise((resolve, reject) => {
      tag.addEventListener("load", () => resolve());
    });
    futures = [...futures, future];
    document.head.appendChild(tag);
  });

  const stylesheets = [
    "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.min.css",
  ];

  stylesheets.forEach((stylesheet) => {
    const tag = document.createElement("link");
    tag.rel = "stylesheet";
    tag.href = stylesheet;
    document.head.appendChild(tag);
  });

  Promise.allSettled(futures).then(() => $("select").selectize());

  const SCORE = "SCORE";
  const KILLS = "KILLS";
  const DEATHS = "DEATHS";
  const ASSISTS = "ASSISTS";
  const AI_KILLS = "AI KILLS";
  const CAP_SHIP_DAMAGE = "CAP SHIP DAMAGE";
  const DOGFIGHT = "DOGFIGHT";
  const FLEET_BATTLE = "FLEET BATTLE";

  const parseReport = (data) => {
    const lines = data.split("\n").filter((line) => !!line);
    let headerLine = lines[0];
    if (headerLine.startsWith(SCORE)) {
      const spacesToAdd =
        Math.max.apply(
          null,
          lines.slice(3).map((line) => line.indexOf("  "))
        ) + 2;
      for (let i = 0; i < spacesToAdd; i++) {
        headerLine = ` ${headerLine}`;
      }
    }
    const columns = {
      playerName: 0,
      score: headerLine.indexOf(SCORE),
      kills: headerLine.indexOf(KILLS),
      deaths: headerLine.indexOf(DEATHS),
      assists: headerLine.indexOf(ASSISTS),
      aiKills: headerLine.indexOf(AI_KILLS),
      capShipDamage: headerLine.indexOf(CAP_SHIP_DAMAGE),
    };

    const gameType = columns.capShipDamage >= 0 ? FLEET_BATTLE : DOGFIGHT;

    const players = lines.slice(2).map((playerData) => {
      const baseStats = {
        playerName: playerData
          .substring(columns.playerName, columns.score)
          .trim(),
        score: parseInt(
          playerData.substring(columns.score, columns.kills).trim()
        ),
        kills: parseInt(
          playerData.substring(columns.kills, columns.deaths).trim()
        ),
        deaths: parseInt(
          playerData.substring(columns.deaths, columns.assists).trim()
        ),
      };
      if (gameType === FLEET_BATTLE) {
        return {
          ...baseStats,
          assists: parseInt(
            playerData.substring(columns.assists, columns.aiKills).trim()
          ),
          aiKills: parseInt(
            playerData.substring(columns.aiKills, columns.capShipDamage).trim()
          ),
          capShipDamage: parseInt(
            playerData.substring(columns.capShipDamage).trim()
          ),
        };
      }
      return {
        ...baseStats,
        assists: parseInt(playerData.substring(columns.assists).trim()),
      };
    });

    return { gameType, players };
  };

  const processComments = () => {
    const comments = document.querySelector("textarea[name=comments]");
    const { players } = parseReport(comments.value);
    console.log(players);
    players.forEach(({ score, kills, deaths, assists }, i) => {
      const stats = [score, kills, deaths, assists];
      stats.forEach((stat, j) => {
        const input = document.querySelector(
          `input[name=stat${j + 1}${i + 1}]`
        );
        if (input) {
          input.value = stat;
        }
      });
    });
    comments.value = "";
  };

  const button = document.createElement("button");
  button.innerText = "Process Comments";
  button.addEventListener("click", processComments);

  document
    .querySelector("input[type=submit][name=rep_game]")
    .parentElement.append(button);
})();
